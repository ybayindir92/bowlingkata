package com.yasemin.bowlingkata;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class BowlingTests {

    private Bowling bowling;

    @Before
    public void setUp() {
        bowling = new Bowling();
    }

    @Test
    public void shouldCalculateZeroForUnluckyGame() {
        // when
        bowling.roll(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        // then
        assertEquals(bowling.calculateScore(), 0);
    }

    @Test
    public void shouldCalculateForAllOne() {
        // when
        bowling.roll(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        // then
        assertEquals(bowling.calculateScore(), 20);
    }

    @Test
    public void shouldCalculateForSpareFollowedByFive() {
        // when
        bowling.roll(2, 8, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        // then
        assertEquals(bowling.calculateScore(), 20);
    }

    @Test
    public void shouldCalculateForStrikeFollowedByFiveThenTwo() {
        // when
        bowling.roll(10, 5, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        // then
        assertEquals(bowling.calculateScore(), 24);
    }

    @Test
    public void shouldCalculateForGoldenGame() {
        // when
        bowling.roll(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);

        // then
        assertEquals(bowling.calculateScore(), 300);
    }
}
