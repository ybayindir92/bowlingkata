package com.yasemin.bowlingkata;

public class Bowling {

    public static void main(String[] args) {
    }

    private int score = 0;
    private int[] rolls = new int[21];
    private int rollIndex = 0;
    private static final int FRAME_COUNT = 10;

    int calculateScore() {
        rollIndex = 0;
        for (int frame = 0; frame < FRAME_COUNT; frame++) {

            if (isSpare(rollIndex)) {
                score += 10 + rolls[rollIndex + 2];
                rollIndex = rollIndex + 2;
            } else if (isStrike(rollIndex)) {
                score += 10 + rolls[rollIndex + 1] + rolls[rollIndex + 2];
                rollIndex++;
            } else {
                score += rolls[rollIndex] + rolls[rollIndex + 1];
                rollIndex = rollIndex + 2;
            }
        }
        return score;
    }

    void roll(final int... rools) {
        for (int pinsDown : rools) {
            roll(pinsDown);
        }
    }

    private void roll(final int pinsDown) {
        rolls[rollIndex] = pinsDown;
        rollIndex++;
    }

    private boolean isSpare(int rollIndex) {
        return rolls[rollIndex] + rolls[rollIndex + 1] == 10;
    }

    private boolean isStrike(int rollIndex) {
        return rolls[rollIndex] == 10;
    }
}